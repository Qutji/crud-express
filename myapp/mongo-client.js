const USER= process.env.MONGO_USER
const PASSWORD= process.env.MONGO_PASSWORD

const MongoClient = require('mongodb').MongoClient;
const uri = `mongodb+srv://${USER}:${PASSWORD}@crud-express-sqf0f.mongodb.net/test?retryWrites=true&w=majority`;
const client = new MongoClient(uri, { useNewUrlParser: true });
  
module.exports = client;