function user_update(name) {
    var new_name = "";
    var new_age = 0;
    fetch('users', {
        method: 'put',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            'origin_name': name,
            'name': new_name,
            'age': new_age
        })
    })
        .then(response => {
            if (response.ok) window.location.reload()
        })
        .then(data => {
            console.log(data)
        })
}

function user_delete(name) {
    fetch('users', {
        method: 'delete',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            'name': name
        })
    })
        .then(response => {
            if (response.ok) window.location.reload()
        })
        .then(data => {
            console.log(data)
        })
}
