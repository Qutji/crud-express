var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser= require('body-parser');
var mongoClient= require('./mongo-client');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: true}));

// Router
app.use('/', indexRouter);

app.use('/users', usersRouter);
app.post('/users', (req, res) => {
  console.log(req.body);
  mongoClient.connect(err => {
    mongoClient.db("crud-express").collection('users').save(req.body, (err, result) => {
      if (err) return console.log(err)
      console.log('saved to database')
      res.redirect('/users')
    }) 
  });
})
app.put('/users', (req, res) => {
  console.log("user update");
  console.log(req.body);
  mongoClient.db("crud-express").collection('users')
  .findOneAndUpdate({name: req.body.origin_name}, {
    $set: {
      name: req.body.name,
      age: req.body.age
    }
  }, {
    sort: {_id: -1},
    upsert: true
  }, (err, result) => {
    if (err) return res.send(err)
    res.send(result);
  });
})
app.delete('/users', (req, res) => {
  console.log("user delete");
  console.log(req.body);
  mongoClient.db("crud-express").collection('users')
  .findOneAndDelete({name: req.body.name}, (err, result) => {
    if (err) return res.send(err)
    res.send(result)
  })
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
