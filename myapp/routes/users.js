var express = require('express');
var router = express.Router();
var mongoClient= require('../mongo-client');

/* GET users listing. */
router.get('/', function(req, res, next) {
  mongoClient.connect(err => {
    mongoClient.db("crud-express").collection('users').find().toArray((err, result) => {
      if (err) return console.log(err)
      res.render('user', {title: 'User', users: result})
    })
  });
});

module.exports = router;
